# OpenML dataset: Abalone-train

https://www.openml.org/d/40759

**WARNING: This dataset is still in preparation.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Source:

Data comes from an original (non-machine-learning) study: 
Warwick J Nash, Tracy L Sellers, Simon R Talbot, Andrew J Cawthorn and Wes B Ford (1994) 
&quot;The Population Biology of Abalone (_Haliotis_ species) in Tasmania. I. Blacklip Abalone (_H. rubra_) from the North Coast and Islands of Bass Strait&quot;, 
Sea Fisheries Division, Technical Report No. 48 (ISSN 1034-3288) 

Original Owners of Database: 

Marine Resources Division 
Marine Research Laboratories - Taroona 
Department of Primary Industry and Fisheries, Tasmania 
GPO Box 619F, Hobart, Tasmania 7001, Australia 
(contact: Warwick Nash +61 02 277277, wnash '@' dpi.tas.gov.au) 

Donor of Database: 

Sam Waugh (Sam.Waugh '@' cs.utas.edu.au) 
Department of Computer Science, University of Tasmania 
GPO Box 252C, Hobart, Tasmania 7001, Australia 


Data Set Information:

Predicting the age of abalone from physical measurements. The age of abalone is determined by cutting the shell through the cone, staining it, and counting the number of rings through a microscope -- a boring and time-consuming task. Other measurements, which are easier to obtain, are used to predict the age. Further information, such as weather patterns and location (hence food availability) may be required to solve the problem. 

From the original data examples with missing values were removed (the majority having the predicted value missing), and the ranges of the continuous values have been scaled for use with an ANN (by dividing by 200).


Attribute Information:

Given is the attribute name, attribute type, the measurement unit and a brief description. The number of rings is the value to predict: either as a continuous value or as a classification problem. 

Name / Data Type / Measurement Unit / Description 
----------------------------- 
Sex / nominal / -- / M, F, and I (infant) 
Length / continuous / mm / Longest shell measurement 
Diameter / continuous / mm / perpendicular to length 
Height / continuous / mm / with meat in shell 
Whole weight / continuous / grams / whole abalone 
Shucked weight / continuous / grams / weight of meat 
Viscera weight / continuous / grams / gut weight (after bleeding) 
Shell weight / continuous / grams / after being dried 
Rings / integer / -- / +1.5 gives the age in years 

The readme file contains attribute statistics.

#autoxgboost #autoweka

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/40759) of an [OpenML dataset](https://www.openml.org/d/40759). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/40759/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/40759/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/40759/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

